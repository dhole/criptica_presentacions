# .center[Qubes OS: Un sistema operativo razonablemente seguro]

.center[<img src="logo.png" width="120">]

<br>

.center[![](cc.png)

Esta presentación está bajo una [licencia de Creative Commons
Reconocimiento-NoComercial-CompartirIgual 4.0
Internacional.](http://creativecommons.org/licenses/by-nc-sa/4.0/)]

---
## Sobre mi

Eduard aka Dhole <dhole@riseup.net>
```
GPG: 9BC4 8EF8 08DB 91DD 158D  559D 4FA4 57A1 8514 CC63
```

- Miembro de [Criptica](https://www.criptica.org)
- Interesado en el diseño de sistemas operativos seguros.
- Usé Qubes OS diariamente durante 3 meses hace un año para evaluarlo personalmente.
- No hablo en nombre del equipo de Qubes.

---
## Qué es Qubes OS?

[Qubes OS](https://www.qubes-os.org/) es un sistema operativo **enfocado a la
seguridad** aplicando un **modelo de compartimentación**, que permite mantener
aislados los datos personales separados por temáticas.

Esto se consigue mediante el uso de **máquinas virtuales (VMs)** para cada temática
(denominada dominio o qube).

Este diseño consigue que el **compromiso de un dominio (y por lo tanto de sus
datos) no afecte a los otros**.

<br>
.center[<img src="logo.png" width="120">]

???
Todo esto es asumiendo que el atacante no tiene forma de escapar de la maquina virtualizada hacia el hypervisor.  Aunque mucho mas difícil que hacer una escalada de privilegios de usuario a root en los sistemas operativos mas comunes, es un ataque posible.

---
## Ejemplo

Por ejemplo, se puede tener un dominio de **trabajo** para leer y mandar emails, un dominio para hacer **compras** online y otro para **navegación casual** por Internet.  Estos dominios se llaman AppVMs.

En caso que nos llegara un malware procedente de la navegación casual por Internet,
éste no tendría acceso a nuestros emails ni a nuestra tarjeta de crédito.

.center[<img src="Qubes_security_domains.png" width="400">]

???
En el dominio de compras tendríamos la seguridad que cuando ponemos el numero de la tarjeta de crédito para comprar, este no podría ser leído por un malware que nos pudiera entrar por otro dominio mas expuesto a inseguridades, como el de navegación casual.

---
## La interfaz gráfica

Todas las aplicaciones funcionando en diferentes dominios (VMs) se muestran en ventanas individuales dentro de un **único entorno de escritorio**.

Para facilitar la comprensión del dominio en el se ejecuta una aplicación, Qubes la dibujará con **bordes de diferente color**.

.center[<img src="r2b2-kde-three-domains-at-work.png" width="600">]


---
## Interacciones entre dominos

- Copia de archivos
- Portapapeles (copiar y pegar)
- Otros (GPG, USB, transferencia de actualizaciones, etc.)

.center[<img src="r2b2-copy-to-other-appvm-1.png" width="680">]

---
## Dominios especiales

#### **dom0**

- Administra el sistema y ejecuta el entorno de escritorio.
- No está conectado a la red.

#### **sys-net**

- Único dominio conectado directamente a Internet.
- Ejecuta los drivers de la tarjeta de red (Ethernet y Wi-Fi)

#### **sys-firewall**

- Comunica al resto de dominios con sys-net, filtrando según las reglas configuradas.

#### **sys-usb** (ver [BadUSB](https://arstechnica.com/information-technology/2014/07/this-thumbdrive-hacks-computers-badusb-exploit-makes-devices-turn-evil/))

- Realiza la comunicación y ejecución de drivers en dispositivos USB.
- O controla la redirección del USB a otro dominio.

???
Un agujero de seguridad en los drivers Wi-Fi, en caso de ser explotado, no permitiría al atacante acceder a ningún dato privado de nuestro ordenador.  Como mucho podría interceptar la conexión a la red.

---
## Dominios desechables (DispVMs)

Se trata de dominios ligeros que son **creados al instante para realizar una
función efímera**, y que se eliminan automáticamente al cerrarse.

Ejemplo: Leer un archivo PDF descargado de una fuente no confiable.

.center[<img src="r2b2-open-in-dispvm-3.png" width="600">]

???
Muy útiles para abrir archivos descargados de Internet.

---
## Whonix

Whonix es un proyecto para conectarse a Tor utilizando 2 VMs:

- **Gateway**: Esta VM enruta todo el trafico a través de Tor.
- **Workstation**: Esta VM esta únicamente conectada a la **Gateway** y se usa para trabajar des de Tor.

Ninguna conexión de la **Workstation** podrá salir a Internet sin pasar por
Tor.  Un ataque a la **Workstation** no podría revelar nuestra IP.

.center[<img src="Whonix_concept_refined.jpg" width="600">]

---
## Split GPG

Con [Split GPG](https://www.qubes-os.org/doc/split-gpg/) se simula el concepto
de una smartcard GPG (como por ejemplo un Yubikei) por software mediante VMs.

Tendremos un dominio aislado de la red con nuestra llave privada GPG que realizará operaciones de firma y cifrado para otros dominios autorizados.

.center[<img src="split-gpg-diagram.png" width="560">]

---
## Requerimientos mínimos según la web oficial

- CPU 64 bits con soporte para virtualitzación moderna (Intel VT-x i VT-d o
  equivalente para AMD)
- 4 GB RAM (siendo realista: 8 GB)
- 32 GB disco

---
## Contras

- No hay aceleración gráfica en las AppVM (por razones de seguridad).
- Reducción de la duración de la batería en portátiles (tendremos como mínimo 5 VMs corriendo a la vez).
- Mayor consumo de memoria RAM (por las múltiples VMs).
- Requiere un cambio en la forma de usar el sistema operativo respecto el modelo tradicional (por la separación de los datos en dominios).

---

# Parte técnica

---
## Virtualitzación en Qubes: el hypervisor

- Qubes usa [Xen](https://xenproject.org/) para virtualitzar
- Xen (pronunciado /ˈzɛn/) és un Hypervisor
- [El concepto de Qubes es generalizable a modelos de compartimentación](https://blog.invisiblethings.org/2018/01/22/qubes-air.html).

.center[<img src="XEN-schema.png" width="400">]

---

## Sistema gráfico

X11, el sistema de ventanas más común en entornos Unix (Linux) permite que cualquier aplicación pueda\*:
- Leer el **portapapeles** cuando quiera.
- Interceptar el **teclado** cuando quiera (keylogger).
- Capturar la **pantalla** cuando quiera.

En Qubes, el entorno gráfico (y X11) se ejecuta en dom0.

Mediante un protocolo, las AppVM dibujan sus ventanas en un entorno
virtualizado des de el que no pueden acceder a los otros dominios.

\* *[Wayland mejora algunos de estos aspectos][1]*
  [1]: https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)#Wayland_Security_Module
---

## TemlateVM

Las AppVMs de Qubes están basadas en plantillas
([TemplateVM](https://www.qubes-os.org/doc/templates/)) que es una instalación
base de un sistema Linux.

Varias AppVMs pueden reutilizar la misma imagen base, que Qubes solo expondrá
en modo lectura.  

Las particiones `/rw`, `/home` y `/usr/local` tienen una copia privada para
cada AppVM en modo escritura.

Para actualizar las AppVM solo hace falta actualizar la TemplateVM en la cual están basadas una única vez.

- Oficiales: Fedora, Debian
- Comunidad: Whonix, Ubuntu, Archlinux

---
## HVM vs PV

Qubes soporta dos modos de virtualización: HVM y PV.

**Hardware Virtual Machine (HVM)** permite virtualizar completamente un sistema
operativo sin modificar, de forma que el sistema operativo cree que se está
ejecutando en hardware real (que está emulado por el sistema de virtualización).
Requiere extensiones de virtualizacion en la CPU.

**Para Virtualization (PV)** requiere una modificación en el sistema operativo, que
será consciente de que está siendo virtualizado.  Ofrece mejor rendimiento ya
que no requiere emulación del hardware.  No requiere extensiones de
virtualización de la CPU.

*PV solo está disponible para Linux, NetBSD y FreeBSD.*

???
HVM permite la virtualización de sistemas operativos Microsoft Windows.

---
## Fin

Preguntas y discusión
