# .center[Vulnerabilidades de Buffer Overflow]

### .center[Una breve introducción]
<br>

.center[![](cc.png)

Esta presentación está bajo una [licencia de Creative Commons
Reconocimiento-NoComercial-CompartirIgual 4.0
Internacional.](http://creativecommons.org/licenses/by-nc-sa/4.0/)]

---
## Sobre mi

Eduard aka Dhole <dhole@riseup.net>

- Miembro de [Criptica](https://www.criptica.org)
- Me gusta saber como funcionan las cosas.
- Tengo interés en la ingenieria inversa aunque no me dedico a ello.

---
## Memoria de un programa

<img style="float:right" src="images/code-data-segments.png"/>

#### Data (+ bss)

Zona de memoria fija donde se guardan los objetos que existen durante todo el programa.

#### Heap

Zona de memoria variable donde se guardan los objetos de tamaño variable.

#### Stack

Zona de memoria que apila "frames" de cada función que se llama.

---
<img style="height: 30em; float:right" src="images/call_stack.png"/>

## Stack

Cada vez que se llama una función se prepara un "frame" nuevo (apilado después
del frame anterior) en el stack para dicha función.

Cuando la función retorna su "frame" se descarta.

En este frame encontramos:
- Argumentos pasados a la función
- Dirección de retorno de la función llamadora
- variables locales

---
## Stack en C

<table style="text-align:left; float: right; width: 30%" border=1>
<tr><th>0xffffff</th></tr>
<tr><th>...</th></tr>
<tr style="background-color: yellow;"><td>v</td></tr>
<tr style="background-color: cyan;"><td>y = 2</td></tr>
<tr style="background-color: cyan;"><td>x = 1</td></tr>
<tr style="background-color: cyan;"><td>return addr main</td></tr>
<tr style="background-color: cyan;"><td>main FP</td></tr>
<tr style="background-color: cyan;"><td>q</td></tr>
<tr style="background-color: cyan;"><td>p</td></tr>
<tr><th>...</th></tr>
<tr><th>0x000000</th></tr>
</table>


<pre style="width: 45%">
<code>
char
add_double(char x, char y) {
  char p = x + y;
  char q = p * 2;
  return q;
}

int
main() {
  char v;
  v = add_double(1, 2);
  printf("%d\n", v);
  return 0;
}
</code>
</pre>

---
## Buffer Overflow

Como funcionan los arrays en C?

<table style="text-align:left; float: right; width: 30%" border=1>
<tr><th>0xffffff</th></tr>
<tr><th>...</th></tr>
<tr style="background-color: cyan;"><td>b[3]</td></tr>
<tr style="background-color: cyan;"><td>b[2]</td></tr>
<tr style="background-color: cyan;"><td>b[1]</td></tr>
<tr style="background-color: cyan;"><td>b[0]</td></tr>
<tr style="background-color: cyan;"><td>a[3]</td></tr>
<tr style="background-color: cyan;"><td>a[2] = 8</td></tr>
<tr style="background-color: cyan;"><td>a[1]</td></tr>
<tr style="background-color: cyan;"><td>a[0]</td></tr>
<tr><th>...</th></tr>
<tr><th>0x000000</th></tr>
</table>

<pre style="width: 45%">
<code>
char a[4];
char b[4];
a[2] = 8; // *(a + 2) = 8
</code>
</pre>

Que pasa si accedemos a `a[4]`?

<pre style="width: 45%">
<code>
a[4] = 42;
</code>
</pre>

---
## Buffer Overflow

Como funcionan los arrays en C?

<table style="text-align:left; float: right; width: 30%" border=1>
<tr><th>0xffffff</th></tr>
<tr><th>...</th></tr>
<tr style="background-color: cyan;"><td>b[3]</td></tr>
<tr style="background-color: cyan;"><td>b[2]</td></tr>
<tr style="background-color: cyan;"><td>b[1]</td></tr>
<tr style="background-color: red;"><td>b[0] = 42</td></tr>
<tr style="background-color: cyan;"><td>a[3]</td></tr>
<tr style="background-color: cyan;"><td>a[2] = 8</td></tr>
<tr style="background-color: cyan;"><td>a[1]</td></tr>
<tr style="background-color: cyan;"><td>a[0]</td></tr>
<tr><th>...</th></tr>
<tr><th>0x000000</th></tr>
</table>

<pre style="width: 45%">
<code>
char a[4];
char b[4];
a[2] = 8; // *(a + 2) = 8
</code>
</pre>

Que pasa si accedemos a `a[4]`?

<pre style="width: 45%">
<code>
a[4] = 42; // *(a + 4) = 42
</code>
</pre>

---
## Explotación

Leer / escribir más allá del tamaño de un array produce el acceso a zonas de
memoria possiblemente no deseadas en el diseño del programa.

Si el programa escribe en los arrays inputs controlados por el atacante que
provocan overflow, el atacante puede controlar el contenido de objetos no
esperados!

A parte de sobreescribir otras variables, puede llegar a sobreescribir la
dirección de retorno, pudiendo controlar el flujo del programa y llegar a la
ejecución de codigo controlado por el usuario.

---
## Explotación

Existen ciertas funciones de la libreria C que copian de un array a otro, como por ejemplo:
- `char *strcpy(char *dest, const char *src)`: copia los caracteres de `src` a
  `dest` hasta encontrar un caracter nulo.

<table style="text-align:left; float: right; width: 30%" border=1>
<tr><th>0xffffff</th></tr>
<tr><th>...</th></tr>
<tr style="background-color: cyan;"><td>*buf</td></tr>
<tr style="background-color: cyan;"><td>return addr</td></tr>
<tr style="background-color: cyan;"><td>caller FP</td></tr>
<tr style="background-color: cyan;"><td>copy[127]</td></tr>
<tr style="background-color: cyan;"><td>copy[126]</td></tr>
<tr style="background-color: cyan;"><td>...</td></tr>
<tr style="background-color: cyan;"><td>copy[1]</td></tr>
<tr style="background-color: cyan;"><td>copy[0]</td></tr>
<tr><th>...</th></tr>
<tr><th>0x000000</th></tr>
</table>

Que pasa cuando `dest` es mas pequeño que el `src`?

<pre style="width: 45%">
<code>
void
foo(char *buf)
{
  char copy[128];
  strcpy(copy, buf);
  ...
}
</code>
</pre>

Si el atacante crea un `buf` con codigo malicioso hasta la posicion 127, y en
la posicion que sobreescribe `return addr` pone la direccion de `copy[0]`,
cuando la función retorne ejecutará el codigo del atacante!

---
## Funciones peligrosas

- `strcpy`
- `strcat`
- `sprintf`
- `gets`

Existen alternativas que limitan el numero de bytes copiados:

- `strncpy`
- `strncat`
- `snprintf`
- `fgets`

---
## Mitigaciones
Pueden evitar el desastre en caso de buffer overflow.

#### Stack Canaries

Incluyen un valor "impredecible" antes de la dirección de retorno con un check
que termina el programa si este valor ha sido cambiado al retornar de la
función.

#### W^X

No permitir que una zona de memoria pueda ser de escritura y ejecución a la
vez.  (Al intentar ejecutar una zona de memoria de escritura el programa es
terminado por el sistema operativo).

#### ASLR

Aleatorizar las posiciones de los objetos en memoria para dificultar que el
atacante pueda aprobechar el buffer overflow.

---
## Evitar Buffer Overflow

Usar un lenguaje *memory safe*: No permite escribir a traves de una variable al
contenido más allá de su memoria asociada.

Ejemplos: python, go, rust, javascript.

---
## Bonus

Quedate en la sesión práctica para intentar resolver un **CTF** (Capture The Flag)
que requiere aprobechar una vulnerabilidad de Buffer Overflow para ser
solucionado!

<img style="width: 100%" src="images/clippy.png">
