# .center[Taller de navegación y comunicaciones anónimas con Tor]

### Organizado por [Críptica](https://www.criptica.org)

- Carlos aka Charlie <charliecr@openmailbox.org>
```
GPG: 482C 38B8 4B20 31AC AE16  9998 8CB3 54D5 D5BD 6F90
```

- Eduard aka Dhole <dhole@openmailbox.org>
```
GPG: 9BC4 8EF8 08DB 91DD 158D  559D 4FA4 57A1 8514 CC63
```

---
# Qué y quien es Críprica
.center[![](cabecera-web.png)]
- Asociación sin ánimo de lucro, nacida en el entorno de la UPC
- Filosofía: Nuestros derechos se tienen que respetar en internet también
- Tras Snowden vemos que ni leyes ni políticas pueden garantizar esto
- Queremos concienciar sobre la importancia de proteger nuestra privacidad
- Organizamos talleres y charlas privacidad y seguridad completamente gartuitos
- Contacto: https://criptica.org
---
# Criterio

<table border="1" style="width:100%">
  <tr>
    <td>Concepto</td>
    <td>Óptimo</td>		
    <td>Subóptimo</td>
    <td>Inaceptable</td>
  </tr>
  <tr>
    <td>Garantías de Privacidad</td>
    <td>Por diseño</td>		
    <td>Por política</td>
    <td>Ninguna</td>
  </tr>
  <tr>
    <td>Tipo de software</td>
    <td>Codigo abierto</td>		
    <td> - </td>
    <td>Privativo</td>
  </tr>
  <tr>
    <td>Cifrado</td>
    <td>Extremo a extremo</td>		
    <td>Punto a punto</td>
    <td>Ninguno</td>
  </tr>
  <tr>
    <td>Tipo de sistema</td>
    <td>Descentralizado</td>		
    <td>Federado o centralizado</td>
    <td> - </td>
  </tr>
  <tr>
    <td>Protección</td>
    <td>Datos y metadatos</td>		
    <td>Sólo Datos</td>
    <td>Ninguna</td>
  </tr>
  <tr>
    <td>Usabilidad y seguridad</td>
    <td>Usable y segura por defecto</td>		
    <td>Segura pero poco usable</td>
    <td>No segura</td>
  </tr>
</table>

---

# Herramientas que veremos hoy

#### Tor Browser: Navegador Web con Tor integrado.
- Windows, OS X, GNU/Linux: https://www.torproject.org/download/download-easy.html
- Fuentes: https://gitweb.torproject.org/

#### Ricochet: Mensajeria instantánea segura.
- Windows, OS X, GNU/Linux: https://ricochet.im/
- Fuentes: https://github.com/ricochet-im/ricochet

#### OnionShare: Comparticion de ficheros de forma anónima.
- Windows, OS X, Ubuntu: https://onionshare.org/
- Debian: `$ sudo apt install onionshare`
- Fuentes: https://github.com/micahflee/onionshare
---

## Qué es Tor

Tor ("The Onion Router"), es un proyecto cuyo objetivo principal es el
desarrollo de una red de comunicaciones distribuida, superpuesta sobre internet,
en la que el encaminamiento de los mensajes intercambiados entre los usuarios
no revela su identidad ni el contenido de los mensajes.

.center[![](tor_logo_small.png)]

#### Cómo funciona:
- Tor: https://www.torproject.org/about/overview.html.en
- Navegación con Tor y HTTPS: https://www.eff.org/pages/tor-and-https

---

# Tor Browser

Tor Browser es una versión de Firefox configurada para conectarse a internet anónimamente a través de Tor. También permite acceder a páginas censuradas regionalmente.

Además, está configurado para esconder información que podria diferenciar un navegador de otro (versión, sistema operativo, idioma, uso horario, ...) y por lo tanto revelar nuestra huella.

### Instalación
- Windows, OS X, GNU/Linux: https://www.torproject.org/download/download-easy.html
- Fuentes: https://gitweb.torproject.org/

---

## CloudFlare y Tor ([#FuckCloudFlare](https://twitter.com/hashtag/fuckcloudflare))

CloudFlare es un _Content Delivery Network_ (CDN). Su red permite proteger, acelerar y mejorar la disponibilidad de las páginas que lo tienen contratado.

CloudFlare considera el trafico procedente de la red Tor como potencialmente malicioso y bloquea las conexiones con uno o varios captcha, supuestamente para frenar este trafico malicioso.

Desgraciadamente este bloqueo afecta a los navegantes que quieren mantenerse anónimos usando Tor, dificultando la navegación.  Esto es especialmente grave con conexiones lentas.

### Ejemplos
- https://medium.com/
- https://pccomponentes.com/
- https://www.meetup.com/
- http://www.fgc.cat/

Qué dice CloudFlare? [The Trouble with Tor](https://blog.cloudflare.com/the-trouble-with-tor/)

Qué dice Tor? [The Trouble with CloudFlare](https://blog.torproject.org/blog/trouble-cloudflare)

---

background-image: url(cloudflare1.png)

---

background-image: url(cloudflare2.png)

---

# Hidden services en Tor

Los hidden services són servicios accesibles solo a través de Tor escondiendo su localización: nadie puede conocer la IP que hay detrás del servicio.

### Ejemplos
- duck duck go (buscador) http://3g2upl4pq6kufc4m.onion/html/
- criptica.org (nuestra pagina) http://w7ehkkt2uuxy77bi.onion/

---

# Ricochet

Ricochet és un servicio de mensajeria instantanea anónima y privada.

El modelo de Ricochet consiste en no confiar en ningun servidor para proteger la privacidad del usuario.
- _Elimina metadatos_. Nadie sabe quien eres, con quien hablas ni lo que dices.
- _Te mantiene anonimo_. Comparte lo que quieras sin revelar tu identidad ni localización.
- _Nadie en medio_. No hay servidores que monitorizar, censurar ni hackear.
- _Seguro por defecto_. Conversaciones cifradas automáticamente.

---

# Como funciona Ricochet

Ricochet crea un __hidden service__ para cada cliente, usada por tus contactos para comunicarse contigo sin revelar tu localización ni dirección IP.

El identificador de usuario és una dirección unica tipo `ricochet:rs7ce36jsj24ogfw`. Otros usuarios de Ricochet pueden usar esta dirección para enviar una solicitud de contacto.

La lista de contactos es __local__ (nunca se expone a servidores ni monitorización de trafico)

Todo va cifrado de _extremo-a-extremo_ y anonimizado. Solo el destinatario puede descifrar los mensajes, y nadie puede saber de donde vienen ni a donde van.

---

# Ricochet

### Instalación
- Windows, OS X, GNU/Linux: https://ricochet.im/
- Fuentes: https://github.com/ricochet-im/ricochet

### Probémoslo

Para ejecutarlo des del explorador de ficheros hace falta bajarse el archivo [ricochet.desktop](ricochet.desktop) y guardarlo en la carpeta de ricochet.  Este archivo contiene información para la ejecución de ricochet como aplicación de escritorio.

Podeis compartir vuestra dirección ricochet en [este pad](https://hyrax.criptica.org/pad/p/private_2016-04-24_taller_ricochet) para agregar a los otros participantes del taller y chatear.

---

# OnionShare

OnionShare es una herramienta que te permite compartir de forma segura y anónima ficheros de cualquier tamaño.

### Como funciona

OnionShare funciona montando un servidor web y haciendolo accesible solo des de un hidden service que se crea temporalmente, finalmente los ficheros se comparten generando una URL aleatoria.

No necesita configurar ningún servidor externo ni usar un servicio de compartición de ficheros de terceros.  

Los archivos residen en tu máquina y són temporalmente accesibles en internet a través del Tor hidden service.  

El otro usuario tan solo necesita Tor Browser para descargar el archivo de tu máquina.

---

# OnionShare

### Instalación
- Windows, OS X, Ubuntu: https://onionshare.org/
- Debian:

`$ sudo apt install onionshare`
- Fuentes: https://github.com/micahflee/onionshare

### Uso
Para usar OnionShare hace falta tener encendido el servicio de Tor. Eso lo podemos conseguir por ejemplo teniendo abierto el Tor Browser.
