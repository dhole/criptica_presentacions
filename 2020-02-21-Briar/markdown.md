# .center[Briar: La alternativa]

.center[<img src="briar-logo.png" width="120">]

<br>

.center[![](cc.png)]

Esta presentación está bajo una [licencia de Creative Commons
Reconocimiento-NoComercial-CompartirIgual 4.0
Internacional.](http://creativecommons.org/licenses/by-nc-sa/4.0/)]

---
## Sobre mi

Eduard aka Dhole <dhole@riseup.net>
```
GPG: 9BC4 8EF8 08DB 91DD 158D  559D 4FA4 57A1 8514 CC63
```

- Miembro de [Criptica](https://www.criptica.org)
- Interesado en diseños de protocolos de comunicación seguros y anónimos.
- No hablo en nombre del equipo de Briar.

---
## Qué es Briar

Briar es una aplicación de software libre de mensajeria cifrada para Android.
- Permite establecer comunicación directamente entre móviles y puede funcionar
  sin Internet.  
- Los mensajes se guardan en el móvil y nunca en un servidor.

Por estos motivos Briar es una aplicación adecuada para situaciones adversas en
las que pueden encontrarse activistas, periodistas y cualquiera que necesite
una forma facil, segura y robusta de comunicación.

Las imagenes mostradas se han sacado de la página de Briar: https://briarproject.org.

---
## Comunicación

Briar puede enviar mensajes entre dispositivos mediante Wi-Fi y Bluetooth
(cuando hay proximidad).

En caso de haber Internet los mensajes viajan mediante la red Tor (para no
revelar metadatos a terceros).

En todos los casos los mensajes van cifrados punto a punto.

.center[<img src="diagram_secure.png" width="720">]

---
## Tipos de comunicación

Los mensajes en Briar pueden intercanviarse mediante:
- Chats privados
- Foros públicos
- Blogs

La diferencia entre cada modalidad está en con quien se comparten los mensajes:
- Chats privados: usuarios invitados por el creador
- Foros públicos: usuarios invitados por un miembro
- Blogs: con todos los contactos

---
## Autenticación entre usuarios

Además, para empezar una comunicación se requiere un proceso de autenticación:
- a) El intercanvio de unos identificadores únicos.
- b) El escaneo mutuo de un codigo QR.

---
## Comunicación p2p

La comunicación de mensajes funciona mediante la sincronización de éstos con
los dispositivos connectados (ya sea por Internet o por Bluetoot / Wi-Fi) que
participan en los canales donde existen estos mensajes.

Esto permite que los mensajes se vayan propagando hasta llegar (potencialmente)
a todos los destinatarios.

.center[<img src="diagram_sharing.png" width="720">]

---
## Modelos de amenaza: protección de ataques

- **Vigilancia de metadatos**: Uso de la red Tor.
- **Vigilancia de contenido**: Cifrado punto a punto.
- **Filtro de contenido**: Cifrado punto a punto.
- **Peticiones de retirada de contenido**: Replica de mensajes en cada dispositivo suscrito a un foro.
- **Ataques de denegación de servicio**: No hay servidor central, cada subscriptor de un foro tiene una copia local del contenido.
- **Inaccesibilidad a internet**: Comunicación via Bluetooth y Wi-Fi cuando no hay Internet.

---
## Modelos de amenaza: protección de atacantes

El adversario tiene las siguientes capacidades:
- Monitorización de Internet
- Bloqueo / Intercepción de datos en Internet
- Monitorización limitada de Bluetooth / Wi-Fi
- Bloqueo / Intercepción limitado de Bluetooth / Wi-Fi
- Ataque Sybil
- No acceso a los dispositivos del usuario
- Acceso limitado a la red de confianza de los usuarios
- Incapaz de romper los protocolos criptográficos

---
## Más información

- Briar: Como funciona (contenido original usado para crear estas diapositivas):
https://briarproject.org/how-it-works/

- Detalles técnicos:
https://code.briarproject.org/briar/briar/wikis/home

---
## Fin

Preguntas y discusión

Vamos a probarlo!
