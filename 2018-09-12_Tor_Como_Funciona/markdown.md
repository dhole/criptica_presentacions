# .center[La red de anonimato Tor: como funciona]

.center[<img src="Tor-logo-2011-flat.svg" width="120">]

<br>

.center[![](cc.png)

Esta presentación está bajo una [licencia de Creative Commons
Reconocimiento-NoComercial-CompartirIgual 4.0
Internacional.](http://creativecommons.org/licenses/by-nc-sa/4.0/)]

---
## Sobre mi

Eduard aka Dhole <dhole@riseup.net>
```
GPG: 9BC4 8EF8 08DB 91DD 158D  559D 4FA4 57A1 8514 CC63
```

- Miembro de [Criptica](https://www.criptica.org)
- Interesado en diseños de protocolos de comunicación seguros y anónimos.
- No hablo en nombre del equipo de Tor.

---
## Sobre esta presentación

Esta es una presentación técnica.

### Qué no se explicará:

- Qué és y cómo usar TorBrowser
- Porqué usar Tor

### Qué se explicará:

- Como funciona Tor
- Qué son y como funcionan los Onion services

---
## Qué es Tor

- Tor es una **red de nodos voluntarios** conectados a Internet que siguen un
protocolo que permite a cualquier usuario **establecer conexiones anónimas**.

- La conexión se anonimiza **cifrándose por capas** (de aquí el nombre Tor: The Onion
Router) y **enrutando el trafico por los nodos**.

- Además, dado que el tráfico es cifrado y anonimizado, es útil para **sortear la
censura** local.

---
## Como funciona Tor

.center[<img src="htw1.png" width="720">]

???
- Obtener listado de nodos Tor
- Elige 3 nodos de forma aleatoria para establecer una ruta.
---
## Como funciona Tor

.center[<img src="htw2.png" width="720">]

???
- Se establece una ruta a por los nodos de forma incremental, cifrando cada capa.
- Llaves de cifrado diferentes para cada nodo
- Cada nodo ve trafico entrada y salida "local"
- Ningún nodo individualmente puede saber el recorrido completo (relacionar el cliente con el destino)
- El túnel final permite pasar cualquier trafico TCP
---
## Como funciona Tor

.center[<img src="htw3.png" width="720">]

???
- Cuando el cliente quiere visitar otro destino, genera una nueva ruta.
- Los dos destinos no sabrán que les visita el mismo cliente.
---
## Nota sobre el anonimato en Tor

1. Si una entidad ve el tráfico del nodo de entrada y salida usado en una
  ruta, puede desanonimizar el cliente!
2. Si alguien controla el nodo de entrada y salida o es capaz de manipular el
   tráfico, desanonimizar el cliente es aún más fácil!

Esto se consigue mediante [correlación de
tráfico](https://www.ohmygodel.com/publications/usersrouted-ccs13.pdf).

- Tor ha ido implementando medidas para dificultar estos ataques, aunque es un
  campo en constante investigación y desarrollo.

---
## Métricas

Los nodos de Tor se pueden consultar mediante algunos servicios web:

- Consulta de nodos por característica (por ejemplo, por país):
https://metrics.torproject.org/rs.html#search/country:es

- Servicio alternativo para consultar la lista de nodos públicos:
http://torstatus.blutmagie.de/

- Consulta el numero de usuarios por país en un rango temporal:
https://metrics.torproject.org/userstats-relay-country.html?start=2017-06-01&end=2017-12-31&country=es&events=on

---
### Qué son los Onion services

(Antiguamente llamados Hidden services)

- Permite al servicio (a diferencia de solo el cliente) ocultar su localización.
- Utiliza la red Tor para enrutar el tráfico entre cliente y servidor sin que
  ninguno de los dos revele su dirección IP.

---
### Como funcionan los Onion services

.center[<img src="tor-onion-services-1.png" width="720">]

???
- El servidor genera un par de llaves asimétricas
- El servidor elige unos nodos al azar
- Les pide a los nodos que hagan de puntos introductorios facilitándoles su llave publica
- La conexión a los nodos es por Tor (anónima)
---
### Como funcionan los Onion services

.center[<img src="tor-onion-services-2.png" width="720">]

???
- El servidor construye el Onion service descriptor:
  - Nodos introductorios
  - Llave pública
  - Onion ID
  - Firma de todo con la llave privada
- Lo sube al directorio DHT
- El Onion ID se construye con un hash de la llave publica creando un sistema de nombres descentralizado y seguro. 
- La conjetura de Zooko dice que en un sistema de nombres solo se pueden tener dos propiedades entre:
  - Descentralización
  - Seguro
  - Memorable
---
### Como funcionan los Onion services

.center[<img src="tor-onion-services-3.png" width="720">]

???
- El cliente se quiere conectar al servicio y sabe su Onion ID
- Consulta el descriptor al directorio DHT usando el Onion ID
- Selecciona un nodo rendezvouz y le envía un secreto efimero
  publica del Onion service
---
### Como funcionan los Onion services

.center[<img src="tor-onion-services-4.png" width="720">]

???
- El cliente se conecta al nodo introductorio del Onion service y entrega los detalles del nodo rendezvouz y el secreto efímero, todo cifrado con la llave pública del Onion service.
- El nodo introductorio entrega esta información al servidor

---
### Como funcionan los Onion services

.center[<img src="tor-onion-services-5.png" width="720">]

???
- El servidor descifra el mensaje recibido por el nodo introductorio.
- El servidor se conecta al nodo rendezvouz y le manda el secreto efimero del cliente.
---
### Como funcionan los Onion services

.center[<img src="tor-onion-services-6.png" width="720">]

???
- El nodo rendezvouz notifica al cliente de la conexión establezida y finaliza el circuito entre cliente y servidor enrutando el tráfico en los dos sentidos.
---
## Como interactuar con Tor

- **Proxy SOCKS**: Tunela cualquier tráfico TCP a través de Tor

- **Tor control**: Controla el proceso Tor, consulta información de la red Tor,
  registra Onion services, etc.

- **torrc**: Archivo de configuración del daemon Tor (`man tor`).
---
## Librerías

Existen varias librerías que permiten interactuar con Tor y configurar Onion
services fácilmente:

### Bine (Golang)
- URL: https://github.com/cretz/bine
- Doc: https://godoc.org/github.com/cretz/bine

### Stem (Python)
- URL: https://stem.torproject.org/index.html

???
- Probaremos estas librerias en el taller!
---
## Más información

Los estándares usados en el protocolo Tor son abiertos y se pueden consultar
para entender los detalles más técnicos:

- Especificación de Tor control port
https://gitweb.torproject.org/torspec.git/tree/control-spec.txt

- Especificación de Onion services V3
https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt

---
## Fin

Preguntas y discusión

En unos minutos empieza la hacking sessión donde **aprenderemos a programar
servicios anónimos con Tor!**
